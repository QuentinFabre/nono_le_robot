package View;

import java.util.ArrayList;
import java.util.List;

import Object.Node;
import Object.Point2D;
import Object.Tree;
import Object.Workshops;

public class ConsoleView {


	public static List<String> getWorkshop(boolean[][] wk) {
		List<String> ls = new ArrayList<String>();
		for(int i =0;i<wk.length;i++) {
			StringBuilder str = new StringBuilder();
			for(int y =0;y<wk[0].length;y++) {	
				str.append( wk[i][y]? " " : "-");
			}
			ls.add(str.toString());
		}
		return ls;
	} 

	 public static void treeToString(Workshops wk,Tree<Point2D> tree) {
 		StringBuilder strBuilder = new StringBuilder();
		List<String> result = getWorkshop(wk.getMatrix());
		tree.getNodes().forEach(value->{
			strBuilder.setLength(0);
			strBuilder.append(result.get(value.getValue().getPointX()));
			strBuilder.setCharAt(value.getValue().getPointY(),'x');
			result.set(value.getValue().getPointX(), strBuilder.toString());
		});
			result.add(0, "Chemin parcouru par l'algorithme");
			result.add(0, wk.getName());
		return;
	
	}
	public static void getPath(Workshops wk,Tree<Point2D> tree) {
		StringBuilder strBuilder = new StringBuilder();
		List<String> result = getWorkshop(wk.getMatrix());
		Node<Point2D> node = tree.getPoint(wk.getEnd());
		while(!node.isRoot()){
			strBuilder.setLength(0);
			strBuilder.append(result.get(node.getValue().getPointX()));
			strBuilder.setCharAt(node.getValue().getPointY(),'X');
			result.set(node.getValue().getPointX(), strBuilder.toString());
			node = node.getParent();
		}
		result.add(0, "Chemin le plus court trouver par l'algorithme");

		result.add(0,wk.getName());
		printList(result);
		return ;
	}

	public static void printList(List<String> list) {
		list.stream().forEachOrdered(s-> System.out.println("		"+s));
	}

}
