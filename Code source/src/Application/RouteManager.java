package Application;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import Algorithm.Walktrough;
import Object.Point2D;
import Object.Tree;
import Object.Workshops;
import Parser.MapFromTextFile;


public class RouteManager    {

	/**
	 * Path to the folder containing workshops files
	 */
	public final Path path = Paths.get(System.getProperty("user.dir")+"\\map\\");
	/**
	 * handle list of workshops
	 */
	private  List<Workshops> ListWorkshop;
	
	/**
	 * Instqnce of the class
	 */
	public static RouteManager unique;
	
	private Walktrough<Point2D> current;

	
	public List<Workshops> getListWorkshop() {
		return ListWorkshop;
	}

	public void setListWorkshop(List<Workshops> listWorkshop) {
		ListWorkshop = listWorkshop;
	}

	/**
	 * @return unique instance of RouteManagers
	 */
	public  static RouteManager getInstance() {
		if(unique==null) {
			unique = new RouteManager(); 
		}
		return unique;
	}
 
	private RouteManager(){
		ListWorkshop = new ArrayList<Workshops>();
		MapFromTextFile map = new MapFromTextFile();
		for(File folder : path.toFile().listFiles()) {
			try {
				ListWorkshop.add(map.GetMap(folder.toPath()));
			} catch (Exception e) {
				System.err.println("Fichier introuvalble");
				e.printStackTrace();
			}
		}
	}

	public Tree<Point2D> getTree(String name){
		Workshops wk = getWork(name);
		return current.Path(wk);
	}
	
	public Workshops getWork(String name) {
		return ListWorkshop.parallelStream().filter(s-> s.getName().equals(name)).findFirst().get();
	}
	
	
	public void setCurrentAlgo(Walktrough<Point2D> wl) {
		current = wl;
	}
}


