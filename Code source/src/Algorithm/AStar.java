package Algorithm;
import java.util.LinkedList;
import java.util.List;
import Object.Node;
import Object.Point2D;
import Object.Tree;
import Object.Workshops;
import java.util.stream.Collectors;

/**
 * @author Thibaut
 * A* algorithm
 */ 
public class AStar   implements Walktrough<Point2D> {


	Point2D start,end;

	Tree<Point2D> closedList ;


	/**
	 * Sorted linked list 
	 */
	List<Node<Point2D>>  openList = new LinkedList<Node<Point2D>> ();

	/* (non-Javadoc)
	 * @see Algorithm.Walktrough#Path(Object.Workshops)
	 */
	@Override
	public Tree<Point2D> Path(Workshops workshop) {
		//Get Start and end of workshops
		openList.clear();
		closedList = new Tree<Point2D>();

		start = workshop.getStart();
		end = workshop.getEnd();

		// set current node <- start
		Node<Point2D> node =  new Node<Point2D>(start);
		node.setParent(node);
		closedList = new Tree<Point2D> (node);
		addNodeSon(node,workshop);
		while(!node.getValue().equals(end) && !openList.isEmpty()){
			node = getBestNode() ;
			addClosedList(node);
			addNodeSon(node,workshop);
		}
		return closedList;



	}

	/**
	 * Get all node who arent in openList
	 * @param inputNode
	 * @param workshop
	 */
	void addNodeSon(Node<Point2D> inputNode,Workshops workshop) {
		//For each point2D not in closedList
		workshop.nextStep(inputNode.getValue()).stream().filter(s->closedList.contain(s)==null).forEach(s->{
			//Get node from openlist or null
			Node<Point2D> nodetemp = get(s);
			//if nodetemps in openlist
			if(nodetemp !=null) {
				if(getDistance(nodetemp)>getDistance(inputNode)+1) {
					nodetemp.setParent(inputNode);
				}
			}else {
				openList.add(new Node<Point2D>(inputNode,s));
				sortList();
			}
		});
	}



	private Node<Point2D> get(Point2D input) {
		try {
			return openList.stream().filter(s->s.getValue().equals(input)).findAny().get();
		}catch(Exception e) {
			return null;
		}
	}


	//  calc rectilinear distance in the grid 
	private int getDistance(Node<Point2D> start) {
		return Math.abs(end.getPointX()-start.getValue().getPointX())+Math.abs(end.getPointY()-start.getValue().getPointY())+start.getNumberParent();
	}



	private void sortList() {
		// Sort the list by comparing distance between two node
		openList =  this.openList.parallelStream().sorted((o1,o2)->Integer.compare(getDistance(o1), getDistance(o2))).collect(Collectors.toList());
	}

	private Node<Point2D> getBestNode(){
		return openList.get(0);
	}

	private void addClosedList(Node<Point2D>  entS) {
		closedList.addNode(entS);
		openList.remove(entS);
	}




}


