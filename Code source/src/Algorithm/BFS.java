package Algorithm;

import java.util.LinkedList;
import Object.Node;
import Object.Point2D;
import Object.Tree;
import Object.Workshops;

/**
 * @author Quentin Fabre
 *
 * 
 */
public class BFS  implements Walktrough<Point2D> {

	@Override
	public Tree<Point2D> Path(Workshops wk) {
		Point2D start = wk.getStart();
		Tree<Point2D> tree = new Tree<Point2D>();
		LinkedList<Point2D> checked = new LinkedList<Point2D>();
		LinkedList<Node<Point2D>> list = new LinkedList<Node<Point2D>>();
		Node<Point2D> node = new Node<Point2D>(start);
		list.add(node);
		tree.addNode(node);
		node.setParent(node);
		checked.add(node.getValue());
		while(!list.isEmpty()) {
			node = list.pop();
			for(Point2D next : wk.nextStep(node.getValue())) {
				if(!contain(next,checked)) {
					checked.add(next);
					Node<Point2D> tmp = new Node<Point2D>(node,next);
					list.add(tmp);
					node.addChild(tmp);
					tree.addNode(tmp);
					if(tmp.getValue().equals(wk.getEnd())) {
						list.clear();
					}
				}
			}

		}
		return tree;
	}
	private boolean contain(Point2D input,LinkedList<Point2D>  hm) {
		return hm.parallelStream().anyMatch(s->s.equals(input));
	}



}
