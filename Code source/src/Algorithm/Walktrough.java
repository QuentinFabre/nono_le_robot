package Algorithm;

import Object.Tree;
import Object.Workshops;

public interface Walktrough <T extends Comparable<T>>  {
	 public Tree<T> Path(Workshops wk); 
}
