package Algorithm;
import java.util.HashSet;
import Object.Node;
import Object.Point2D;
import Object.Tree;
import Object.Workshops;

/**
 * @author Thibaut
 * Implementation of DFS algorithm
 */
public class DFS  implements Walktrough<Point2D> {

	/**
	 * Result of the algorithm
	 */
	private Tree<Point2D>  treeWk ;
	private HashSet<Point2D> checked ;
	@Override
	public Tree<Point2D> Path(Workshops wki) {
		treeWk =  new Tree<Point2D>();
		checked = new HashSet<Point2D>();
		explore(wki.getStart(),null,wki);
		return treeWk;
	}
	

	
	private void  explore(Point2D point,Node<Point2D> parent,Workshops wki){
			Node<Point2D> node = new Node<Point2D>(parent, point);
			treeWk.addNode(node);
			checked.add(point);
			for(Point2D nextPoint : wki.nextStep(point)) {
				if(!contain(nextPoint)) {
					explore(nextPoint,node,wki);
				}
			}
	}
	
	private boolean contain(Point2D input) {
		return checked.parallelStream().anyMatch(s->s.compareTo(input)==0);
	}

	
}
