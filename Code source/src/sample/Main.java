package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.Event;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import Algorithm.AStar;
import Algorithm.BFS;
import Algorithm.DFS;
import Application.RouteManager;
import Object.Point2D;
import Object.Tree;
import Object.Workshops;

public class Main extends Application  {

	private List<String> mapsPath;//Temporaire
	private RouteManager Route;
	private String pathFile;
	Path path = Paths.get(System.getProperty("user.dir")+"\\map\\");
	String currentwk = null;
	Point2D  CurrePoint;
	Color  CurreColor;
	AStar a = new AStar();
	BFS bfs = new BFS();
	DFS dfs = new DFS();


	GridDynamic grid;
	@Override
	public void start(Stage primaryStage) throws Exception{

		mapsPath = new ArrayList<>();
		BorderPane root = FXMLLoader.load(getClass().getResource("sample.fxml"));
		primaryStage.setTitle("Atelier Nono le robot");
		GridDynamic grid = new GridDynamic(80, 80); // Paramètres ---> affecter taille matrice map
		Scene scene = new Scene(root, 1000, 700, Color.GRAY);
		root.getChildren().add(grid.makeGrid(scene));

		primaryStage.getIcons().setAll(new Image(getClass().getResource("3ilogo.png").toExternalForm()));

		MenuBar menuBar = new MenuBar();
		menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
		root.setTop(menuBar);

		Menu algorithmMenu = new Menu("Algorithmes");
		MenuItem BFSMenuItem = new MenuItem("BFS");
		MenuItem DFSMenuItem = new MenuItem("DFS");
		MenuItem starMenuItem = new MenuItem("A_star");
		RouteManager Route = RouteManager.getInstance();



		BFSMenuItem.setOnAction(event -> {
			Route.setCurrentAlgo(bfs);
			if(currentwk != null) {
				Tree<Point2D>  tmp = Route.getTree(currentwk);
				grid.Draw(Route.getWork(currentwk).getMatrix());
				grid.DrawTree(tmp);
				grid.DrawPath(tmp, Route.getWork(currentwk).getEnd());
			}
		});


		DFSMenuItem.setOnAction(event -> {
			Route.setCurrentAlgo(dfs);
			if(currentwk != null) {
				Tree<Point2D> tmp = Route.getTree(currentwk);
				grid.Draw(Route.getWork(currentwk).getMatrix());
				grid.DrawTree(tmp );
				grid.DrawPath(tmp , Route.getWork(currentwk).getEnd());
			}
		});

		starMenuItem.setOnAction(event -> {
			Route.setCurrentAlgo(a);
			if(currentwk != null) {
				Tree<Point2D> tmp = Route.getTree(currentwk);
				grid.Draw(Route.getWork(currentwk).getMatrix());
				grid.DrawTree(tmp);
				grid.DrawPath(tmp, Route.getWork(currentwk).getEnd());
			}
		});

		algorithmMenu.getItems().addAll(BFSMenuItem, DFSMenuItem, starMenuItem, new SeparatorMenuItem());

		Menu fileMenu = new Menu("Fichier");
		MenuItem openMenuItem = new MenuItem("Ouvrir fichier");

		openMenuItem.setOnAction(event -> {
			FileChooser chooser = new FileChooser();
			File file = chooser.showOpenDialog(primaryStage);
			if (file != null) {
				pathFile = file.toString();
				if(getExtensionOfFile(pathFile)) {
				}
				else
					errorExtensionFile(pathFile);
			} else {
				pathFile = null;
			}
		});

		fileMenu.getItems().addAll(openMenuItem, new SeparatorMenuItem());

		menuBar.getMenus().addAll(fileMenu, algorithmMenu);

		Menu newMenu = new Menu(getNamePath("Ouvrir"));
		List<MenuItem> stockItem = new ArrayList<>();



		for (Workshops map : Route.getListWorkshop()){
			MenuItem newMenuItem = new Menu(map.getName());
			stockItem.add(newMenuItem);
		}


		for(MenuItem item : stockItem){
			item.setOnAction(event -> {
				grid.makeGrid(scene);
				grid.Draw(Route.getWork("Atelier"+item.getText().replaceAll("\\D+", "")).getMatrix());
				currentwk = "Atelier"+item.getText().replaceAll("\\D+", "");
				primaryStage.show();
			});
			newMenu.getItems().addAll(item, new SeparatorMenuItem());
		}

		menuBar.getMenus().addAll(newMenu);
		primaryStage.setScene(scene);

		primaryStage.show();
	}



	private List<String> temporyMethod(){ // Temporaire
		for(File folder : path.toFile().listFiles()) {
			try {
				mapsPath.add(folder.toPath().toString());
			} catch (Exception e) {
				System.err.println("Fichier introuvalble");
				e.printStackTrace();
			}
		}
		return mapsPath;
	}

	private boolean getExtensionOfFile(String pathFile)
	{
		String fileExtension = "";

		if(pathFile.contains(".") && pathFile.lastIndexOf(".")!= 0)
		{
			fileExtension = pathFile.substring(pathFile.lastIndexOf(".") + 1);
		}

		return fileExtension.equals("txt");
	}

	private void errorExtensionFile(String filePath){
		Alert bteDialog = new Alert(Alert.AlertType.ERROR);
		bteDialog.setTitle("ERROR");
		bteDialog.setHeaderText(null);
		bteDialog.setContentText("Le fichier " + getNamePath(filePath) + " doit être au format .txt");
		bteDialog.showAndWait();
	}

	private String getNamePath(String pathFile){
		int index = pathFile.lastIndexOf("\\");
		String nameFile = pathFile.substring(index + 1);
		return nameFile;
	}

	public String getPathFile(){
		return pathFile;
	}

	public static void main(String[] args) {
		launch(args);

	}





}
