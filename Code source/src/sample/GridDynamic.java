package sample;

import java.util.Observable;
import java.util.Observer;

import Object.Node;
import Object.Point2D;
import Object.Tree;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class GridDynamic extends Application implements Observer{
	
    private Rectangle[][] rec;
    private Pane p;
    private int heightPane, widthPane;

    public GridDynamic(int heightPane, int widthPane){
        this.heightPane = heightPane;
        this.widthPane = widthPane;
        this.rec = new Rectangle[this.widthPane][this.heightPane];
        this.p = new Pane();
    }

    
    public Rectangle[][] getRec() {
		return rec;
	}

    
	public void setRec(Rectangle[][] rec) {
		this.rec = rec; 
	}
	 public void Draw(boolean[][] tab) {
	    	GridDynamic grid = new GridDynamic(tab[0].length,tab.length);
	    	for(int x = 0;x<tab[0].length;x++) {
	    	for(int i = 0; i<tab.length;i++) {
	    			rec[x][i].setFill(tab[i][x]?Color.WHITE:Color.BLACK);
	    		}
	    	}
	    }
	 
	 public void Drawcase(Point2D point,Color color) {
		 rec[point.getPointY()][point.getPointX()].setFill(color);
		
	 }
 
	 public void DrawTree(Tree<Point2D> tree) {
		 try {
		 tree.getNodes().forEach(value->{
				rec[value.getValue().getPointY()][value.getValue().getPointX()].setFill(Color.BLUE);
			});
		 }catch(Exception e) {
			 System.out.println("Pas de solutions trouv�");
		 }
	 }
	 
	 
	 public void DrawPath(Tree<Point2D> tree,Point2D end) {
			Node<Point2D> node = tree.getPoint(end);
			while(!node.isRoot()){
				rec[node.getValue().getPointY()][node.getValue().getPointX()].setFill(Color.RED);
				node = node.getParent();
			}
	 } 
	public Pane makeGrid(Scene scene) {
        double width = scene.widthProperty().doubleValue()/widthPane;
        double height = scene.heightProperty().doubleValue()/heightPane;
 
        for(int i=0; i<widthPane; i++){
            for(int j=0; j<heightPane; j++){
                rec[i][j] = new Rectangle();
                rec[i][j].setX(i * width);
                rec[i][j].setY(j * height);
                rec[i][j].setWidth(width);
                rec[i][j].setHeight(height);
                rec[i][j].setFill(Color.WHITE);
                rec[i][j].setStroke(Color.BLACK);
                p.getChildren().add(rec[i][j]);
            }
        }

        p.setLayoutY(26.0);//Adaptation grille sous MenuBar

        return p;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

    }

  


	@Override
	public void update(Observable arg0, Object arg1) {
			
			
	}
}
