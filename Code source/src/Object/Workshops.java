package Object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List; 

public class Workshops implements Explorable<Point2D> {
	private boolean[][] matrix;
	Point2D end,start;
	String name;
	public Workshops(boolean[][] matrix, Point2D end, Point2D start,  String name) {
		super();
		this.matrix = matrix;
		this.end = end;
		this.start = start;
		this.name = name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean[][] getMatrix() {
		return matrix;
	}
	public Point2D getEnd() {
		return end;
	}
	public Point2D getStart() {
		return start;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Workshops"+System.lineSeparator() + "matrix=" + Arrays.deepToString(matrix)+ System.lineSeparator() + " end=" + end+  System.lineSeparator() + " start=" + start  + " name=" + name ;
	} 
	@Override
	public List<Point2D> nextStep(Point2D point) {
		int x = point.getPointX();
		int y = point.getPointY();
		List<Point2D> result = new ArrayList<Point2D>();
		int pos = -1;
		for(int i = 0;i<2;i++) {
			if(isAvailable(x+pos, y)) {
				result.add(new Point2D(x+pos, y, true));
			}
			if(isAvailable(x, y+pos)) {
				result.add(new Point2D(x, y+pos, true));
			}
			pos = 1;
		}
		return result;
	}
	
	
	@Override
	public boolean isArrived(Point2D point) {
		return point == end;
	}

	boolean isAvailable(int x,int y){
		if(x>=0 && x<=matrix.length-1 && y>=0 && y<=matrix[0].length-1) {
			return (matrix[x][y]);
	}
		return false;
		}

	public boolean isEmpty(Point2D[] arr) {

		for(Point2D point : arr) {
			if(point!= null) return false;
		}
		return true;
	}



}
