package Object;

public class Point2D implements Comparable<Point2D>{
	@Override
	public String toString() {
		return "Point2D [pointX=" + pointX + ", pointY=" + pointY + ", value=" + value + "]";
	}
	private int pointX;
	private int pointY;
	private boolean value;


	public int getPointX() {
		return pointX;
	}
	public int getPointY() {
		return pointY;
	}
	public boolean isValue() {
		return value;
	}
	public Point2D(int pointX, int pointY, boolean value) {
		this.pointX = pointX;
		this.pointY = pointY;
		this.value = value;
	}
	@Override
	public int compareTo(Point2D point) {
		if(this.pointX!=point.pointX || this.pointY!=point.pointY) return -1;
 		return 0;
	}	
	
	public boolean equals(Point2D input) {
		return this.pointX==input.pointX && this.pointY==input.pointY;
	}
}
