package Object;

import java.util.HashSet;
import java.util.NoSuchElementException;

/**
 * @author Quentin Fabre
 * class who represent Tree structure
 * and Node searching
 */
public class Tree<T extends Comparable<T>> {

    private Node<T> root;
    private HashSet<Node<T>> nodes ;    
    
    
    public HashSet<Node<T>> getNodes() {
		return nodes;
	}
    
	public Node<T> contain(T input) {
		try{
	
			return this.nodes.parallelStream()
					.filter(x -> x.getValue().compareTo(input)==0)
					.findFirst()
					.get();

		}catch(NoSuchElementException e){
			return null;
		}
	}

	public void setNodes(HashSet<Node<T>> nodes) {
		this.nodes = nodes;
	}


	public Node<T> getPoint(T input) {
		return nodes.parallelStream().filter(s-> s.getValue().compareTo(input)==0).findFirst().get();
	}

    public Tree(){
    	nodes = new HashSet<Node<T>>();
        this.root = null;
    }

    public Tree(Node<T> node){
    	nodes = new HashSet<Node<T>>();

        this.root = node;
    }

    public Node<T> getRoot() {
    	return this.root;
    }

    
    public void addNode(Node<T> parent){
    	if (this.nodes.isEmpty())root = parent;
    	nodes.add(parent);
    }

    

    
    public int getSize() {
    	return nodes.size();
    }
}
