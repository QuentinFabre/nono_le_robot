package Object;

import java.util.List;

public interface Explorable<T>  {

	List<T> nextStep(T point);
		
		boolean isArrived(T point);
}
