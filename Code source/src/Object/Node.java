package Object;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Thibaut
 * class representing Tree node
 */
public class Node<T> {
	private List<Node<T>> children;
	private Node<T> parent;
	private T value;
	
	public Node(Node<T> parent, T value) {
		children = new ArrayList<Node<T>>();
		this.parent = parent;
		this.value = value;
	}
	
	
	public Node(T value) {
		children = new ArrayList<Node<T>>();
		this.value = value;
	}
	public void addChild(Node<T> obj) {
		if(this.children.contains(obj)) {
			return;
		}
		this.children.add(obj);
	}
	
	public void setParent(Node<T> par) {
		try {
		this.parent.children.remove(par);
		}catch(Exception e) {	
		}finally {
			this.parent = par;
		}
	}

	
	public Node<T> getParent() {
		return parent;
	}
	
	public T getValue() {
		return value;
	}
	
	public boolean isLeaf() {
		return this.children.isEmpty();
	}
	
	public void addChildren(Node<T> node) {
		children.add(node);
	}
	
	   public boolean isRoot() {
	    	return (this.getParent()==null||this.getParent()==this)?  true :false;
	    }
	   
	public int getNumberParent() {
		Node<T> tmp = this.getParent();
		int r = 2;
		while(tmp.getParent()!=tmp) {
			r++;
			tmp = tmp.getParent();
		}
		return r;
	}
	
	
	public void setChildren(List<Node<T>> input) {
		children = input;
	}
	public List<Node<T>> getChildren() {
		return this.children;
	}
	
	public void setChildren(Node<T> node) {
		this.children = new ArrayList<Node<T>>();
		this.children.add(node);
	}



	
}
