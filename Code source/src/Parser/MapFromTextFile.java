package Parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Supplier;
import java.util.stream.Stream;

import Object.Point2D;
import Object.Workshops;

/**
 * @author Thibaut
 *
 */
public class MapFromTextFile implements MapReader<Path> {

	
	/* (non-Javadoc)
	 * @see Parser.MapReader#GetMap(java.lang.Object)
	 */
	@Override
	public  Workshops GetMap(Path input) throws IOException {
		String Mapname;
		Point2D end,start;

		Supplier<Stream<String>> streamSupplier = () -> {
			try {
				return Files.lines(input);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		};
		//Get mapname
		Mapname = streamSupplier.get().skip(0).findFirst().get();
		//GET Start
		int[] coord = getCoord(streamSupplier.get().skip(1).findFirst().get());
		start = new Point2D(coord[0],coord[1],true);
		//GET END
		coord = getCoord(streamSupplier.get().skip(2).findFirst().get());
		end = new Point2D(coord[0],coord[1],true);
		//GET SIZE
		coord = getCoord(streamSupplier.get().skip(3).findFirst().get());
		//GET MATRIX
		String[] stringArray = streamSupplier.get().skip(4).toArray(String[]::new);
		boolean[][] matrix = getMatrix(stringArray,coord[0],coord[1]);

		return new Workshops(matrix,end,start,Mapname);
	}

	/**
	 * Parse string and get 2 numbers separate by a blank
	 * @param str
	 * @return array of int containing the numbers
	 */
	private int[] getCoord(String str){
		String[] Coord = str.split("\\s+");
		int[] result = new int[2];
		result[0] = Integer.parseUnsignedInt(Coord[0]);
		result[1] = Integer.parseUnsignedInt(Coord[1]);
		return result;
	}

	/**
	 * @param lines 
	 * @param nbRow
	 * @param nbCol
	 * @return boolean[][] representing a workshop
	 */
	private boolean[][] getMatrix(String[] lines,int nbRow,int nbCol){
		//getMatrix
		boolean[][] matrix = new boolean[nbRow][nbCol];
		for(int y = 0 ;y<nbRow;y++) {
			for(int i = 0;i<nbCol;i++) { 
			//test if the char is an empty one or not
				matrix[y][i] = (lines[y].charAt(i)!=' ')?false:true;
			}

			//increase the number of the line
		}
	return matrix;
}

}


