package Parser;

import Object.Workshops;

/**
 * @author Thibaut
 * Infeface who instantiate a workshop from object
 * @param <T>
 */
public  interface MapReader<T> {
	
	 Workshops GetMap(T input) throws Exception  ;

}
